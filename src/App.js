import { useEffect, useState } from "react";
import "./App.css";
import { db } from "./config";
import {
    collection,
    addDoc,
    Timestamp,
    query,
    orderBy,
    onSnapshot,
    updateDoc,
    doc,
    deleteDoc,
} from "firebase/firestore";

function App() {
    const [state, setState] = useState({
        name: "",
        address: "",
    });
    const [data, setData] = useState([]);
    const [selected, setSelected] = useState({});

    const getData = () => {
        const q = query(collection(db, "users"), orderBy("created", "desc"));
        onSnapshot(q, (querySnapshot) => {
            setData(
                querySnapshot.docs.map((doc) => ({
                    id: doc.id,
                    data: doc.data(),
                }))
            );
        });
    };

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const onSubmit = async (e) => {
        e.preventDefault();
        try {
            if (Object.keys(selected).length) {
                await updateDoc(doc(db, "users", selected.id), {
                    name: state.name,
                    address: state.address,
                });
                setSelected([]);
                setState({
                  name: "",
                  address: "",
                });
                getData();
            } else {
                await addDoc(collection(db, "users"), {
                    name: state.name,
                    address: state.address,
                    created: Timestamp.now(),
                });
                getData();
                setState({
                    name: "",
                    address: "",
                });
            }
        } catch (err) {
            console.log(err);
        }
    };

    const onDelete = async (id) => {
        try {
            await deleteDoc(doc(db, "users", id));
            setSelected([]);
            getData();
        } catch (err) {
            console.log(err);
        }
    };

    return (
        <div className="flex flex-col h-screen w-screen justify-center items-center">
            <div>
                <form className="flex flex-col mb-10" onSubmit={onSubmit}>
                    <input
                        type="text"
                        placeholder="Masukkan Nama"
                        className="border border-slate-400 py-2 px-4 rounded-lg mb-4"
                        value={state.name}
                        onChange={(e) =>
                            setState({ ...state, name: e.target.value })
                        }
                    />
                    <input
                        type="text"
                        placeholder="Masukkan Alamat"
                        className="border border-slate-400 py-2 px-4 rounded-lg mb-4"
                        value={state.address}
                        onChange={(e) =>
                            setState({ ...state, address: e.target.value })
                        }
                    />
                    <button
                        type="submit"
                        className="bg-blue-600 rounded-lg py-2 text-white"
                    >
                        {Object.keys(selected).length ? "Edit" : "Simpan"}
                    </button>
                </form>
            </div>
            <div className="flex">
                {data.map((item) => {
                    return (
                        <div
                            key={item.id}
                            className="group flex items-center w-64 justify-between border border-slate-400 rounded-lg px-4 py-3 mr-2 cursor-pointer hover:bg-blue-500 group-hover:text-white"
                        >
                            <div>
                                <p className="group-hover:text-white">{item.data.name}</p>
                                <p className="group-hover:text-white">{item.data.address}</p>
                            </div>
                            <div>
                                <p
                                    className="text-xs text-blue-500 group-hover:text-white mt-2"
                                    onClick={() => {
                                        setSelected(item);
                                        setState({
                                            ...state,
                                            name: item.data.name,
                                            address: item.data.address,
                                        });
                                    }}
                                >
                                    Edit
                                </p>
                                <p
                                    className="text-xs text-red-600 mt-2"
                                    onClick={() => onDelete(item.id)}
                                >
                                    Delete
                                </p>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}

export default App;
