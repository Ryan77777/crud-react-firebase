// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyB7i2fl8FJS9DEVKwy6L8QkrksRJZkVpYg",
  authDomain: "crud-react-97922.firebaseapp.com",
  databaseURL: "https://crud-react-97922-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "crud-react-97922",
  storageBucket: "crud-react-97922.appspot.com",
  messagingSenderId: "59975447837",
  appId: "1:59975447837:web:b45b7238f53bafb9b85df4"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore(app)

export {db}